(function (angular) {
  "use strict";

  var app = angular.module('myApp.report', ['firebase.auth','ngRoute', 'firebase.utils', 'firebase', 'ui.bootstrap']);
    

  app.controller('ReportCtrl', ['$scope', 'PublishersService', 'GroupsService', function($scope, PublishersService, GroupsService) {
        var todaydate  = new Date();
        var currentMonth = todaydate.getMonth();
      $scope.monat = {
      
          monat : currentMonth+1
             
      };
      
     
   $scope.data = {static1: false,static2: false, static3: true};
   
   $scope.isCollapsed = true;

    
      $scope.filtermonth = function(actual, expected){
        return  actual == expected;
          };
   
      $scope.forms = {};
  
      $scope.publishers = PublishersService.getPublishers();
      $scope.addPublisher = function () {
          
        var newPublisher =  angular.copy($scope.forms.newPublisher);
        
           PublishersService.addPublisher(newPublisher);
       
          };
           
      $scope.updatePublisherg = function (id) {
            
          PublishersService.updatePublisherg(id);
         
        
      };
     
      
       $scope.updatePublisher = function (id) {
              console.log(id);
          
          PublishersService.updatePublisher(id);
         
        
      };
      
      
          
         
       $scope.removePublisher = function (id) {
          PublishersService.removePublisher(id);
        

       };
    //groups
          
   
      
       $scope.groups = GroupsService.getGroups();
      $scope.addGroup= function () {
          
        var newGroup =  angular.copy($scope.forms.newGroup);
       
            GroupsService.addGroup(newGroup);

     };
      
      $scope.updateGroup = function (id) {
          GroupsService.updateGroup(id);
        
      };
       $scope.removeGroup = function (id) {
          GroupsService.removeGroup(id);
        

       };
      
      
      
  
    }]);
    
  
  app.factory('PublishersService', ['$firebaseArray', 'FBURL', function($firebaseArray, FBURL) {
    
      
      
 var ref = new Firebase(FBURL+"/congregation");
      var authData = ref.getAuth();
 var uidref= ref.child(authData.uid)
  var refPublisher= uidref.child('publisher')
 
      
      
      
    
      var publishers = $firebaseArray(refPublisher);
      
       
    
      var getPublishers = function() {
          return publishers;
      };
        var addPublisher = function(publisher) {
           publishers.$add(publisher).then(function(ref) {
  var id = ref.key();
  console.log("added publisher with id " + id);
    
               
});
             
      
          
          
      };
      
      
  
      
          var updatePublisherg = function(id) {
              
              var index = publishers.$indexFor(id);
              console.log(index)
                
        publishers.$save(index) ;
        
      };
      
      
       var updatePublisher = function(id) {
         
        publishers.$save(id)   
          console.log(  publishers.$save(id) );
      };
    
  
       var removePublisher = function(id) {
         publishers.$remove(id);
      };
      
      return {
          getPublishers: getPublishers,
          addPublisher: addPublisher,
          updatePublisher: updatePublisher,
 updatePublisherg: updatePublisherg,
     
          removePublisher: removePublisher
          
      };
      
  }]);
    
      app.factory('GroupsService', ['$firebaseArray', 'FBURL', function($firebaseArray, FBURL) {
    
      
      
 var ref = new Firebase(FBURL+"/congregation");
      var authData = ref.getAuth();
 var uidref= ref.child(authData.uid)
  var refGroup= uidref.child('group')
      var groups = $firebaseArray(refGroup);
      
      var getGroups = function() {
          return groups;
      };
        var addGroup = function(group) {
         groups.$add(group);
      };
      
       var updateGroup = function(id) {
        groups.$save(id)
      };
      
       var removeGroup = function(id) {
         groups.$remove(id);
      };
      
      return {
          getGroups: getGroups,
          addGroup: addGroup,
          updateGroup: updateGroup,
          removeGroup: removeGroup
          
      };
      
  }]);


  app.config(['$routeProvider', function($routeProvider) {
    $routeProvider.whenAuthenticated('/report', {
      templateUrl: 'report/report.html',
      controller: 'ReportCtrl'
    });
  }]);

})(angular);