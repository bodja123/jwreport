(function(angular) {
  'use strict';
var app =  angular.module('myApp.navi', ['firebase.auth', 'firebase', 'firebase.utils', 'ngRoute']);

    app.directive('acmeNavbar', acmeNavbar);
    
    app.controller('NavController', function ($scope, $location) {
    $scope.isCollapsed = true;
    $scope.$on('$routeChangeSuccess', function () {
        $scope.isCollapsed = true;
        $scope.getClass = function (path) {
    if(path === '/') {
        if($location.path() === '/') {
            return "active";
        } else {
            return "";
        }
    }
 
    if ($location.path().substr(0, path.length) === path) {
        return "active";
    } else {
        return "";
    }
}
    });
});

  /** @ngInject */
  function acmeNavbar() {
    return {
      templateUrl: 'components/navbar/navbar.html',
        
         resolve: {
        // forces the page to wait for this promise to resolve before controller is loaded
        // the controller can then inject `user` as a dependency. This could also be done
        // in the controller, but this makes things cleaner (controller doesn't need to worry
        // about auth status or timing of accessing data or displaying elements)
        user: ['Auth', function (Auth) {
          return Auth.$waitForAuth();
        }]
      }
      }; 
    
   

   }

  
      
    /** @ngInject */
 
  


})(angular);